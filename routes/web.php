<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/auth/redirect', function () {
    return Socialite::driver('google')->redirect();
});

Route::get('/auth/callback', function () {
    $user = Socialite::driver('google')->user();

    // $user->token
});

Route::get('/auth/redirect', function () {
    return Socialite::driver('facebook')->redirect();
});

Route::get('/auth/callback', function () {
    $user = Socialite::driver('facebook')->user();

// $user->token
});

Route::get('/auth/callback', function () {
    $user = Socialite::driver('google')->user();

    // Провайдер OAuth 2.0 ...
    $token = $user->token;
    $refreshToken = $user->refreshToken;
    $expiresIn = $user->expiresIn;

    // Провайдер OAuth 1.0 ...
    $token = $user->token;
    $tokenSecret = $user->tokenSecret;

    // Все провайдеры ...
    $user->getId();
    $user->getLogin();
    $user->getEmail();
});

Route::get('/auth/callback', function () {
    $user = Socialite::driver('facebook')->user();

    // Провайдер OAuth 2.0 ...
    $token = $user->token;
    $refreshToken = $user->refreshToken;
    $expiresIn = $user->expiresIn;

    // Провайдер OAuth 1.0 ...
    $token = $user->token;
    $tokenSecret = $user->tokenSecret;

    // Все провайдеры ...
    $user->getId();
    $user->getLogin();
    $user->getName();
    $user->getEmail();
});