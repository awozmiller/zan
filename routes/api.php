<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

use App\Models\Greeting;

use App\Models\Banner;
use App\Models\AboutDescription;
use App\Models\Direction;
use App\Models\Partner;

use App\Models\AssistentDescription;
use App\Models\Lawyer;
use App\Models\Statement;
use App\Models\Law;

use App\Models\HelpDescription;
use App\Models\Psychologist;
use App\Models\Book;
use App\Models\Video;

use App\Models\Seminar;

use App\Models\Article;
use App\Models\Media;
use App\Models\Algorithm;

use App\Models\Story;

use App\Models\HotlineContact;
use App\Models\StateContact;
use App\Models\CenterContact;
use App\Models\CommissionContact;
use App\Models\VolunteerContact;




/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {

});

Route::post('/tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);
    return ['token' => $token->plainTextToken];
});


Route::get('/greeting', function() {
    return response(Greeting::all());
});

Route::get('/legal-advice', function() {
    return response(['descritpion' =>AssistentDescription::first() ,
                     'contacts'=>Lawyer::all(),
                     'statements'=>Statement::all(),
                     'Law'=>Law::first()]);
});

Route::get('/psy', function(){
    return response(['description'=>HelpDescription::first(),
                     'contacts'=>Psychologist::all(),
                     'books'=>Book::all(),
                     'videos'=>Video::all()]);
});

Route::get('/seminars', function(){
    return response(['seminars'=>Seminar::all()]);
});

Route::get('/library', function(){
    return response(['articles'=>Article::all(),
                     'media'=>Media::all(),
                     'algorithms'=>Algorithm::all()]);
});

Route::get('/stories', function(){
    return response(['stories'=>Story::all()]);
});

Route::get('/about', function(){
    return response(['description'=>AboutDescription::first(),
                     'directions'=>Direction::all(),
                     'partners'=>Partner::all()]);
});

Route::get('/contacts', function(){
    return response(['hotline'=>HotlineContact::all(),
                     'state'=>StateContact::all(),
                     'center'=>CenterContact::all(),
                     'commission'=>CommissionContact::all(),
                     'volunteer'=>VolunteerContact::all()]);
});

Route::post('/login',[AuthController::class, 'login']);
Route::post('/register',[AuthController::class, 'register']);
Route::post('logout',[AuthController::class, 'logout']);