<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Support\Facedes\Hash;
use Hash;
use App\Models\User;

use Laravel\Socialite\Facades\Socialite;
class AuthController extends Controller
{
    public function register(Request $request) {
        $fields = $request->validate([
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string',
        ]);

        $user = User::create([
            'email' => $fields ['email'],
            'password' => bcrypt($fields['password'])
        ]);

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token 
        ];

        return response($response, 201);
    }

    // public function login(Request $request) {
    //     $fields = $request->validate([
    //         'email' => 'required|string',
    //         'password' => 'required|string',
    //     ]);

    //     $user = User::where('email', $fields['email'])->first();
    //     if($user || !Hash::check($fields['password'], $user->password)) {
    //         return response([
    //             'message' => 'Неправильный пароль'
    //         ], 401);
    //     }

    //     $token = $user->createToken('myapptoken')->plainTextToken;

    //     $respone = [
    //         'user' => $user,
    //         'token' => $token 
    //     ];

    //     return response($response, 201);
    // }

    public function login(Request $request) {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        // Check email
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Неверный пароль'
            ], 401);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }






    public function logout(Request $request) {
        if ($request->user()) { 
            $request->user()->tokens()->delete();
        }
        return [
            'message' => 'Вы вышли из аккаунта'
        ];
    }
}
